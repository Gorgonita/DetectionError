
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import operationsLogit.DetectionGenerateError;

public class DetectionError extends JFrame {

    private JLabel titulo1 = new JLabel("Ingresa el numero de variables");
    private JLabel titulo2 = new JLabel("Ingresa el generador");
    private JTextField bits = new JTextField();
    private JTextField ecuacion = new JTextField();;
    private JButton start = new JButton("Generar tabla");
    private JButton clear = new JButton("Limpiar tabla");
    private JLabel titulo3 = new JLabel("Ingresa el limite y minimo de bits");
    private JTextField maxmin = new JTextField();
    private JPanel panel = new JPanel();
    private JLabel image = new JLabel("Cargando...");

    // Elementos para mostrar resultados
    private int x = 0, y = 0;
    private JTable table = new JTable();
    private JScrollPane table_scroll;
    private JTable table_e = new JTable();
    private JScrollPane table_scroll_e;

    public DetectionError() {
        this.initComponents();
        this.initialEvents();
    }

    private void initComponents() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(new Dimension(1350, 750));
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setTitle("Genera tus mensaje");
        this.panel.setLayout(null);
        this.titulo1.setBounds(10, 10, 250, 30);
        this.titulo2.setBounds(10, 50, 280, 30);
        this.titulo3.setBounds(670, 10, 250, 30);
        this.maxmin.setBounds(670, 50, 150, 30);
        this.clear.setBounds(830, 50, 150, 30);
        ImageIcon img = new ImageIcon("src/operationsLogit/cargagif.gif");
        ImageIcon icono = new ImageIcon(img.getImage().getScaledInstance(50, 25, Image.SCALE_DEFAULT));
        this.image.setIcon(icono);
        this.image.setBounds(520, 50, 150, 50);
        this.image.setVisible(false);
        this.bits.setBounds(290, 10, 200, 30);
        this.ecuacion.setBounds(290, 50, 200, 30);
        this.start.setBounds(500, 20, 150, 30);
        x = 600;
        y = 600;
        this.table.setPreferredScrollableViewportSize(new Dimension(x + 50, y + 50));
        this.table.setFillsViewportHeight(true);
        this.table_scroll = new JScrollPane(this.table);
        this.table_scroll.setBounds(10, 100,600, y);
        this.table_e.setPreferredScrollableViewportSize(new Dimension(x + 50, y + 50));
        this.table_e.setFillsViewportHeight(true);
        this.table_scroll_e = new JScrollPane(this.table_e);
        this.table_scroll_e.setBounds(620, 100,700, y);
        getContentPane().add(this.table_scroll);
        getContentPane().add(this.table_scroll_e);
        getContentPane().invalidate();
        getContentPane().validate();
        this.panel.add(this.maxmin);
        this.panel.add(this.image);
        this.panel.add(this.titulo3);
        this.panel.add(this.clear);
        this.panel.add(this.start);
        //this.panel.add(scroll_text);
        this.panel.add(this.bits);
        this.panel.add(this.ecuacion);
        this.panel.add(this.titulo1);
        this.panel.add(this.titulo2);
        this.maxmin.setText("(7,3)");
        this.add(this.panel);
    }

    private void initialEvents() {

        this.start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                generarClick(e);
            }
        });
        
        this.clear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clearClick(e);
            }
        });
    }
    
    private void clearClick(ActionEvent e){
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setColumnCount(0);
        model.setRowCount(0);
        model.addColumn(" Mensaje en binario");
        model.addColumn(" Palabras de codigo en binario");
        DefaultTableModel model_e = (DefaultTableModel) table_e.getModel();
        model_e.setColumnCount(0);
        model_e.setRowCount(0);
        model_e.addColumn(" Mensaje en ecuacion ");
        model_e.addColumn(" Palabras de codigo en ecuacion ");
    }

    private void generarClick(ActionEvent evt) {
        Thread hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                image.setVisible(true);
                int var = Integer.parseInt(bits.getText());
                String generate = ecuacion.getText();
                DetectionGenerateError detect = new DetectionGenerateError();
                detect.printTable(table, table_e, var, generate, maxmin.getText());
                image.setVisible(false);
            }
        });
        hilo.start();
    }

    public static void main(String[] args) {
        DetectionError form = new DetectionError();
        form.setVisible(true);

    }

}
