package operationsLogit;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Edwin A. Galicia V.
 * @version 0.1
 */
public class DetectionGenerateError {
    private char[][] elements;
    private char[] result;
    private int column;
    private String equation;
    private final char[] abecedario = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',};
    private final int x = 120, minn = 48, maxn = 57, plus =  43, minus = 45;
    private int[] limits = new int[2];
    
    public DetectionGenerateError() {
//        readerBinary(binary);
//        System.out.println(binary+" = "+this.equation);
//        readerEquation(equation);
//        System.out.println(equation+" = "+this.equation);
//        System.out.println(multiplyEquation(binary, this.equation));
    }

    /**
     * Este metodo realiza la lectura de una ecuacion y genera 
     * la representacion de la ecuacion en binario
     * @param equation 
     */
    private String readerEquation(String equation){
        int c = 0;
        ArrayList<String> positions = new ArrayList<>();
        ArrayList<Integer> bits = new ArrayList<>();
        String num = "";
        equation += String.valueOf((char)plus);
        while (c < equation.length()){
            if (equation.codePointAt(c) >= this.minn && this.maxn >= equation.codePointAt(c)) num += equation.charAt(c);
            if (equation.codePointAt(c) == plus || equation.codePointAt(c) == this.minus){
                if (equation.codePointAt(c - 1) == this.minn + 1 && 1 == Integer.parseInt(String.valueOf(num))){
                    positions.add("0");
                    c++;
                    continue;
                }
                if (equation.codePointAt(c - 1) >= this.minn && this.maxn >= equation.codePointAt(c - 1)){
                    positions.add(num);
                    if (equation.length() - 1 == c) c++;
                    num = "";
                }
                if (equation.codePointAt(c - 1) == this.x) positions.add("1");
            }
            c++;
        }
        c = 0;
        positions.forEach((v) -> { bits.add(Integer.parseInt(v)); });
        positions.clear();
        for (int j = 0; j < bits.get(0) + 1; j++){
            if (c < bits.size()){
                if ((bits.get(0) + 1) - (bits.get(c) + 1) == j){
                    positions.add("1");
                    c++;
                } else positions.add("0");
            } else positions.add("0");
                
        }
        this.equation = "";
        positions.forEach((val) -> { this.equation += val; });
        return this.equation;
    }
    
    /**
     * Este metodo realiza la lectura de una cadena binara
     * y genera la ecuacion de dicho valor binario
     * @param binary 
     */
    private String readerBinary(String binary){
        String expression = "";
        boolean check = false;
        int val = binary.length() - 1;
        for (int c = 0; c < binary.length(); c++){
            if (binary.charAt(c) == '1'){
                if (val - 1 == c) expression = expression + "x";
                else if (val == c) expression = expression + "1";
                else expression = expression + "x^" + (val-c);
                check = true;
            }
            if (c < val && check && !(binary.charAt(c+1) == '0')) {
                expression = expression + "+";
                check = false;
            }
        }
        return expression;
    }
    
    /**
     * Este metodo recibe un argumento llamado binary es una cadena de texto
     * que contiene 0 y 1 y retorna el maximo exponencial que contiene 
     * este binario si fuera una ecuacion
     * @param binary
     * @return Integer
     */
    private int getMaxExponential(String binary){
        int num = 0;
        for (int c = 0; c < binary.length(); c++) {
            if (binary.charAt(c) == '1'){
                num = ((binary.length() - 1) - c);
                break;
            }
        }
        return num;
    }
    
    /**
     * Este metodo me multiplica los polinomios que ingresamos
     * el argumento message es el mensaje y el generador es el argumento exprecion
     * @param message
     * @param exprecion 
     */
    private String multiplyEquation(String message, String exprecion){
        ArrayList<Integer> num = new ArrayList<>();
        int maxEx = getMaxExponential(exprecion);
        for (int i = 0; i < maxEx; i++) message += "0";
        /*for (int c = 0; c < message.length(); c++) 
            if (message.charAt(c) == '1') 
                num.add((message.length() - 1) - c);
        for (int c = 0; c < num.size(); c++) num.set(c, num.get(c) + maxEx);
        maxEx = 0;
        for (int j = 0; j < num.get(0) + 1; j++){
            if (maxEx < num.size()){
                if ((num.get(0) + 1) - (num.get(maxEx) + 1) == j ){
                    binaryResult += "1";
                    maxEx++;
                } else binaryResult += "0";
            } else binaryResult += "0";
        }*/
        return message;
    }
    
    /**
     * Este metodo me hace la divicion de el generador sobre el mensaje multiplicado 
     * por el valor maximo de la ecuacion
     * @param message
     * @param generate
     * @return 
     */
    public String divideEquation(String message, String generate){
        //message = readerBinary(message);
        //message = readerEquation(message);
        boolean check = true;
        String temp = "";
        String tempdivisor = generate;
        while (check){
            if (message.length() >= tempdivisor.length()){
                temp = "";
                int ciclos = message.length() - generate.length();
                for (int c = 0; c < ciclos; c++) generate += "0";
                for (int c = 0; c < message.length(); c++){
                    if (message.charAt(c) == generate.charAt(c)) temp += "0";
                    else temp += "1";
                }
                message = temp;
                if (temp.equals(generate)){
                    message = "";
                    for (int i = 0; i < temp.length(); i++) message += "0";
                    break;
                }
                int c = 0;
                for (int i = 0; i < temp.length(); i++) if (temp.charAt(i) == '0') c++;
                if (c == temp.length()){
                    message = temp;
                    check = false;
                } else {
                    temp = readerBinary(temp);
                    temp = readerEquation(temp);
                    message = temp;
                }
            } else check = false;
        }
        return message;
    }

    private void generateValues(int var) {
        this.column = var;
        int values = 2;
        int n = var;
        for (int c = 1; c < n; c++) values *= 2;
        var = values;
        this.elements = new char[var][values];
        this.result = new char[values];
        for (int c = 0; c < n; c++) {
            values = values / 2;
            boolean check = false;
            for (int i = 0; i < var; i++) {
                if (i % values == 0 && i != 0) check = check == false; 
                if (!check) this.elements[c][i] = '0';
                else this.elements[c][i] = '1';
            }
        }
    }

    public String print() {
        String out = "";
        for (int i = 0; i < this.column; i++) {
            out += ("| " + this.abecedario[i] + " |");
        }
        out += ("| R |\n");
        for (int c = 0; c < this.result.length; c++) {
            for (int i = 0; i < this.column; i++) {
                out += ("| " + this.elements[i][c] + " |");
            }
            out += ("| " + this.result[c] + " |\n");
        }
        return out;
    }

    public void print(JTable table) {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setColumnCount(0);
        model.setRowCount(0);
        for (int i = 0; i < this.column; i++) {
            model.addColumn(" " + this.abecedario[i] + " ");
        }
        model.addColumn(" R ");
        for (int c = 0; c < this.result.length; c++) {
            Object[] row = new Object[this.column + 1];
            for (int i = 0; i < this.column; i++) {
                row[i] = this.elements[i][c];
            }
            row[this.column] = this.result[c];
            model.addRow(row);
        }
    }
    
    public String printTable() {
        String out = "";
        out += "| Mensaje | ";
        out += ("| Palabras de codigo |\n");
        for (int c = 0; c < this.result.length; c++) {
            for (int i = 0; i < this.column; i++) {
                out += ("| " + this.elements[i][c] + " |");
            }
            out += ("| " + this.result[c] + " |\n");
        }
        return out;
    }
    
    public void printTable(JTable table, JTable tableecuation, int var, String generate, String limits){
        defineLimits(limits);
        if (this.limits[1] <=  var){
            generateValues(var);
            String dividendo = "";
            String palabra = "";
            generate = readerEquation(generate);
            DefaultTableModel model = (DefaultTableModel) table.getModel();
            model.setColumnCount(0);
            model.setRowCount(0);
            model.addColumn(" Mensaje en binario");
            model.addColumn(" Palabras de codigo en binario");
            DefaultTableModel model_e = (DefaultTableModel) tableecuation.getModel();
            model_e.setColumnCount(0);
            model_e.setRowCount(0);
            model_e.addColumn(" Mensaje en ecuacion ");
            model_e.addColumn(" Palabras de codigo en ecuacion ");
            for (int c = 0; c < this.result.length; c++) {
                Object[] row = new Object[2];
                Object[] row_e = new Object[2];
                String msn = "";
                for (int i = 0; i < this.column; i++) {
                    msn += this.elements[i][c];
                    //row[i] = this.elements[i][c];
                    row[0] = msn;
                }
                row_e[0] = readerBinary(msn);
                String v = ""; 
                for (int f = 0; f < msn.length(); f++) v += "0";
                if (msn.equals(v)) {
                    int num = this.limits[0] - v.length();
                    for (int i = 0; i < num; i++) msn += "0";
                    row[1] = msn;
                    row_e[1] = msn;
                    model.addRow(row);
                    model_e.addRow(row_e);
                    continue;
                }
                dividendo = this.multiplyEquation(msn, generate);
                palabra = this.divideEquation(dividendo, generate);
                row[1] = getPalabra(palabra, dividendo);
                row_e[1] = readerBinary(getPalabra(palabra, dividendo));
                model.addRow(row);
                model_e.addRow(row_e);
            }
        } else JOptionPane.showMessageDialog(null, "El numero tiene que ser menor o igual a "+this.limits[1]);
    }
    
    private String getPalabra(String palabra, String dividendo){
        String equation = readerBinary(dividendo)+"+"+readerBinary(palabra);
        if (equation.length() == 1){
            equation = "";
            for (int i = 0; i < this.limits[0]; i++) equation += "0";
        } else {
            equation = readerEquation(equation);
            int temp = this.limits[0]-equation.length();
            for (int i = 0; i < temp; i++) equation = "0"+equation;
        }
        return equation;
    }
    
    private void defineLimits(String limits){
        int c = 0;
        String num = "";
        for (int i = 0; i < limits.length(); i++) {
            if (limits.codePointAt(i) >= minn && limits.codePointAt(i) <= maxn){
                num += String.valueOf(limits.charAt(i));
            } else {
                if (num.length() > 0) {
                    this.limits[c++] = Integer.parseInt(num);
                    num = "";
                    if (c == 2) break;
                }
            }
        }
    }
    
    public static void main(String[] args) {
//        DetectionGenerateError v = new DetectionGenerateError(3);
//        v.divideEquation("00011010", "10011");
    }
}
