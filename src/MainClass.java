
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import operationsLogit.ResultVar;

public class MainClass extends JFrame {

    private JLabel titulo1 = new JLabel("Ingresa el numero de variables");
    private JLabel titulo2 = new JLabel("Selecciona un tipo de tabla de verdad");
    private JTextField numero = new JTextField();
    private JComboBox tipotabla = new JComboBox();
    private JTextArea text = new JTextArea();
    private JButton start = new JButton("Generar tabla");
    private JLabel titulo3 = new JLabel("Ingresa que columa deseas invertir");
    private JTextField columna = new JTextField();
    private JPanel panel = new JPanel();

    // Elementos para mostrar resultados
    private int x = 0, y = 0;
    private JTextArea console = new JTextArea();
    private JScrollPane scroll_text;
    private JTable table = new JTable();
    private JScrollPane table_scroll;

    public MainClass() {
        this.initComponents();
        this.initialEvents();
    }

    private void initComponents() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(new Dimension(1350, 750));
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setTitle("Genera tus tablas de verdad");
        this.panel.setLayout(null);
        this.tipotabla.addItem("Seleccionar");
        this.tipotabla.addItem("Conversion");
        this.tipotabla.addItem("AND");
        this.tipotabla.addItem("OR");
        this.tipotabla.addItem("Bicondicional");
        this.tipotabla.addItem("Disyuncion");
        this.tipotabla.addItem("Disyuncion exclusiva");
        this.tipotabla.addItem("Condicional");
        this.text.disable();
        this.titulo1.setBounds(10, 10, 250, 30);
        this.titulo2.setBounds(10, 50, 280, 30);
        this.titulo3.setBounds(670, 10, 250, 30);
        this.columna.setBounds(670, 50, 150, 30);
        this.numero.setBounds(290, 10, 200, 30);
        this.tipotabla.setBounds(290, 50, 200, 30);
        this.start.setBounds(500, 20, 150, 30);
        this.text.setText("En este programa tu podras\nagregar o ingresar un valor de tipo\nnumerico en el campo de texto,\ndonde dice ingresa el numero -\neste tiene que ser un numero que\npueda ser divisible de 2,\na continuacion selecciona un tipo\nde tabla logica,\n1.- Conversion\n2.- AND\n3.- OR\n4.- Bicondicional\n5.- Disyuncion\n6.- Condicional");
        scroll_text = new JScrollPane(this.text, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll_text.setBounds(50, 100, 260, 260);
        this.add(scroll_text);
        x = 400;
        y = 600;
        scroll_text = new JScrollPane(this.console, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        scroll_text.setBounds(350, 100, x, y);
        this.add(scroll_text);
        this.table.setPreferredScrollableViewportSize(new Dimension(x + 50, y + 50));
        this.table.setFillsViewportHeight(true);
        this.table_scroll = new JScrollPane(this.table);
        this.table_scroll.setBounds(350 + x + 10, 100, x + 100, y);
        getContentPane().add(this.table_scroll);
        getContentPane().invalidate();
        getContentPane().validate();
        this.console.setVisible(true);
        this.panel.add(this.columna);
        this.panel.add(this.titulo3);
        this.panel.add(this.start);
        this.panel.add(scroll_text);
        this.panel.add(this.numero);
        this.panel.add(this.tipotabla);
        this.panel.add(this.titulo1);
        this.panel.add(this.titulo2);
        this.columna.setText("0");
        this.add(this.panel);
    }

    private void initialEvents() {

        this.tipotabla.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                comboAction(e);
            }
        });

        this.numero.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                keyEvent(e);
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }
        });

        this.columna.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                keyEvent(e);
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }
        });

        this.start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                generarClick(e);
            }
        });
    }

    private void comboAction(ActionEvent e) {
        if (this.tipotabla.getSelectedIndex() == 1) {
            this.columna.setVisible(true);
            this.titulo3.setVisible(true);
        } else {
            this.columna.setVisible(false);
            this.titulo3.setVisible(false);
        }
    }

    private void keyEvent(KeyEvent kevt) {
        if (48 > kevt.getKeyCode() && 57 < kevt.getKeyCode()) {
            kevt.consume();
        }
    }

    private void generarClick(ActionEvent evt) {
        int value = Integer.parseInt(this.numero.getText());
        if (this.tipotabla.getSelectedIndex() > 0 && this.numero.getText().length() != 0) {
            Thread run = new Thread(new Runnable() {
                @Override
                public void run() {
                    ResultVar var = null;
                    if (tipotabla.getSelectedIndex() == 1) {
                        var = new ResultVar(Integer.parseInt(numero.getText()), tipotabla.getSelectedItem().toString(), Integer.parseInt(columna.getText()));
                    } else {
                        var = new ResultVar(Integer.parseInt(numero.getText()), tipotabla.getSelectedItem().toString());
                    }
                    console.setText(var.print());
                    var.print(table);
                    table.updateUI();
                }
            });
            run.run();
        }

    }

    public static void main(String[] args) {
        MainClass form = new MainClass();
        form.setVisible(true);

    }

}
